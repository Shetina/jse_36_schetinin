package ru.t1.schetinin.tm.repository;

import ru.t1.schetinin.tm.api.repository.ISessionRepository;
import ru.t1.schetinin.tm.model.Session;

public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {
}
