package ru.t1.schetinin.tm.api.repository;

import ru.t1.schetinin.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}