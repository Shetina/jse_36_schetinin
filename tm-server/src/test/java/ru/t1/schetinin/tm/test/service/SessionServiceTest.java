package ru.t1.schetinin.tm.test.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.schetinin.tm.api.repository.ISessionRepository;
import ru.t1.schetinin.tm.api.service.ISessionService;
import ru.t1.schetinin.tm.exception.field.IdEmptyException;
import ru.t1.schetinin.tm.exception.user.UserIdEmptyException;
import ru.t1.schetinin.tm.marker.UnitCategory;
import ru.t1.schetinin.tm.model.Project;
import ru.t1.schetinin.tm.model.Session;
import ru.t1.schetinin.tm.repository.SessionRepository;
import ru.t1.schetinin.tm.service.SessionService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class SessionServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    private static final String USER_ID_FAKE = UUID.randomUUID().toString();

    @NotNull
    private static final List<Session> SESSION_LIST = new ArrayList<>();

    @NotNull
    private static final ISessionRepository SESSION_REPOSITORY = new SessionRepository();

    @NotNull
    private static final ISessionService SESSION_SERVICE = new SessionService(SESSION_REPOSITORY);

    @Before
    public void initDemoData() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = new Session();
            if (i <= 5) session.setUserId(USER_ID_1);
            else session.setUserId(USER_ID_2);
            SESSION_REPOSITORY.add(session);
            SESSION_LIST.add(session);
        }
    }

    @After
    public void clearData() {
        SESSION_LIST.clear();
        SESSION_REPOSITORY.clear();
    }

    @Test
    public void testAddSession() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        final int expectedNumberOfEntriesUser = SESSION_SERVICE.getSize(USER_ID_1) + 1;
        @NotNull final String userId = USER_ID_1;
        @NotNull final Session session = new Session();
        SESSION_SERVICE.add(session);
        SESSION_SERVICE.add(userId, session);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_SERVICE.getSize());
        Assert.assertEquals(expectedNumberOfEntriesUser, SESSION_SERVICE.getSize(USER_ID_1));
        Assert.assertNull(SESSION_SERVICE.add(userId,null));
    }

    @Test
    public void testExistById() {
        @NotNull final Session session = SESSION_LIST.get(0);
        Assert.assertTrue(SESSION_SERVICE.existsById(session.getId()));
        Assert.assertTrue(SESSION_SERVICE.existsById(session.getUserId(), session.getId()));
    }

    @Test
    public void testAddAll() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull final List<Session> sessionNew = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = new Session();
            if (i <= 5) session.setUserId(USER_ID_1);
            else session.setUserId(USER_ID_2);
            sessionNew.add(session);
        }
        SESSION_SERVICE.add(sessionNew);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_SERVICE.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Session> sessions = SESSION_SERVICE.findAll();
        @NotNull final List<Session> sessions2 = SESSION_SERVICE.findAll(USER_ID_1);
        Assert.assertEquals(sessions.size(), SESSION_SERVICE.getSize());
        Assert.assertEquals(sessions2.size(), SESSION_SERVICE.getSize(USER_ID_1));
    }

    @Test
    public void testFindOneById() {
        @NotNull final List<Session> sessions = SESSION_SERVICE.findAll(USER_ID_1);
        @NotNull final Session session1 = sessions.get(0);
        @NotNull final String projectId = session1.getId();
        Assert.assertEquals(session1, SESSION_SERVICE.findOneById(projectId));
    }

    @Test
    public void testFindOneByIndex() {
        @NotNull final Session session1 = SESSION_LIST.get(0);
        @NotNull final Session session2 = SESSION_LIST.get(SESSION_SERVICE.getSize() - 1);
        @NotNull final Session session3 = SESSION_LIST.get(SESSION_SERVICE.getSize() / 2);
        Assert.assertEquals(session1, SESSION_SERVICE.findOneByIndex(0));
        Assert.assertEquals(session2, SESSION_SERVICE.findOneByIndex(SESSION_SERVICE.getSize() - 1));
        Assert.assertEquals(session3, SESSION_SERVICE.findOneByIndex(SESSION_SERVICE.getSize() / 2));
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        SESSION_SERVICE.clear();
        Assert.assertEquals(expectedNumberOfEntries, SESSION_SERVICE.getSize());
    }

    @Test
    public void testClearUser() {
        final int expectedNumberOfEntries = 0;
        SESSION_SERVICE.clear(USER_ID_1);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_SERVICE.getSize(USER_ID_1));
    }


    @Test
    public void testRemove() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Session session = SESSION_LIST.get(0);
        SESSION_SERVICE.remove(session);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_SERVICE.getSize());
    }

    @Test
    public void testRemoveUser() {
        final int expectedNumberOfEntries = SESSION_SERVICE.getSize(USER_ID_1) - 1;
        @NotNull final Session session = SESSION_LIST.get(0);
        SESSION_SERVICE.remove(USER_ID_1, session);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_SERVICE.getSize(USER_ID_1));
        Assert.assertNull(SESSION_SERVICE.remove(USER_ID_1, null));
    }

    @Test
    public void testRemoveById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Session session = SESSION_LIST.get(0);
        SESSION_SERVICE.removeById(session.getId());
        Assert.assertEquals(expectedNumberOfEntries, SESSION_SERVICE.getSize());
    }

    @Test
    public void testRemoveByIdUserNegative() {
        @NotNull final Session session = SESSION_LIST.get(0);
        Assert.assertThrows(IdEmptyException.class, () -> SESSION_SERVICE.removeById(USER_ID_1, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> SESSION_SERVICE.removeById(null, session.getId()));
        Assert.assertNull(SESSION_SERVICE.removeById(USER_ID_FAKE, session.getId()));
    }

    @Test
    public void testRemoveByIndex() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        SESSION_SERVICE.removeByIndex(0);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_SERVICE.getSize());
    }

    @Test
    public void testRemoveByIndexUser() {
        final int expectedNumberOfEntries = SESSION_SERVICE.getSize(USER_ID_1) - 1;
        SESSION_SERVICE.removeByIndex(USER_ID_1, 0);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_SERVICE.getSize(USER_ID_1));
        Assert.assertNull(SESSION_SERVICE.removeByIndex(USER_ID_FAKE, 1));
    }

    @Test
    public void testRemoveAll() {
        final int expectedNumberOfEntries = 0;
        SESSION_SERVICE.removeAll(SESSION_LIST);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_SERVICE.getSize());
    }

    @Test
    public void testSet() {
        @NotNull List<Session> sessions = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            sessions.add(new Session());
        }
        SESSION_SERVICE.set(sessions);
        Assert.assertEquals(sessions, SESSION_SERVICE.findAll());
    }

}