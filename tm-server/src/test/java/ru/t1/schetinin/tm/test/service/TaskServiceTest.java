package ru.t1.schetinin.tm.test.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.schetinin.tm.api.repository.IProjectRepository;
import ru.t1.schetinin.tm.api.repository.ITaskRepository;
import ru.t1.schetinin.tm.api.service.ITaskService;
import ru.t1.schetinin.tm.enumerated.Sort;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.schetinin.tm.exception.entity.TaskNotFoundException;
import ru.t1.schetinin.tm.exception.field.DescriptionEmptyException;
import ru.t1.schetinin.tm.exception.field.IdEmptyException;
import ru.t1.schetinin.tm.exception.field.IndexIncorrectException;
import ru.t1.schetinin.tm.exception.field.NameEmptyException;
import ru.t1.schetinin.tm.exception.user.UserIdEmptyException;
import ru.t1.schetinin.tm.marker.UnitCategory;
import ru.t1.schetinin.tm.model.Project;
import ru.t1.schetinin.tm.model.Task;
import ru.t1.schetinin.tm.repository.ProjectRepository;
import ru.t1.schetinin.tm.repository.TaskRepository;
import ru.t1.schetinin.tm.service.TaskService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class TaskServiceTest {


    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_FAKE = UUID.randomUUID().toString();

    @NotNull
    private static final List<Task> TASK_LIST = new ArrayList<>();

    @NotNull
    private static final ITaskRepository TASK_REPOSITORY = new TaskRepository();

    @NotNull
    private static final List<Project> PROJECT_LIST = new ArrayList<>();

    @NotNull
    private static final IProjectRepository PROJECT_REPOSITORY = new ProjectRepository();

    @NotNull
    private static final ITaskService TASK_SERVICE = new TaskService(TASK_REPOSITORY);

    @Before
    public void initDemoData() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            @NotNull final Task task = new Task();
            project.setName("Project number " + i);
            project.setDescription("Description " + i);
            task.setName("Task number " + i);
            task.setDescription("Description " + i);
            if (i <= 5) {
                project.setUserId(USER_ID_1);
                task.setUserId(USER_ID_1);
                task.setProjectId(project.getId());
            }
            else {
                project.setUserId(USER_ID_2);
                task.setUserId(USER_ID_2);
                task.setProjectId(project.getId());
            }
            PROJECT_REPOSITORY.add(project);
            PROJECT_LIST.add(project);
            TASK_SERVICE.add(task);
            TASK_LIST.add(task);
        }
    }

    @After
    public void clearData() {
        TASK_SERVICE.clear();
        TASK_LIST.clear();
    }

    @Test
    public void testAddTask() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        final int expectedNumberOfEntriesUser = TASK_SERVICE.getSize(USER_ID_1) + 1;
        @NotNull final String userId = USER_ID_1;
        @NotNull final Task task = new Task();
        TASK_SERVICE.add(task);
        TASK_SERVICE.add(userId, task);
        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize());
        Assert.assertEquals(expectedNumberOfEntriesUser, TASK_SERVICE.getSize(USER_ID_1));
        Assert.assertNull(TASK_SERVICE.add(userId,null));
    }

    @Test
    public void testExistById() {
        @NotNull final Task task = TASK_LIST.get(0);
        Assert.assertTrue(TASK_SERVICE.existsById(task.getId()));
        Assert.assertTrue(TASK_SERVICE.existsById(task.getUserId(), task.getId()));
    }

    @Test
    public void testCreateTask() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = USER_ID_1;
        @NotNull final String taskName = "Task create 1";
        @NotNull final Task task = TASK_SERVICE.create(userId, taskName);
        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize());
        Assert.assertEquals(userId, task.getUserId());
        Assert.assertEquals(taskName, task.getName());
    }

    @Test
    public void testCreateTaskWithDesc() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = USER_ID_1;
        @NotNull final String taskName = "Task create 1";
        @NotNull final String description = "Desc task 1";
        @NotNull final Task task = TASK_SERVICE.create(userId, taskName, description);
        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize());
        Assert.assertEquals(userId, task.getUserId());
        Assert.assertEquals(taskName, task.getName());
        Assert.assertEquals(description, task.getDescription());
    }

    @Test
    public void testAddAll() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull final List<Task> taskNew = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task number" + i * 2);
            task.setDescription("Description " + i * 2);
            if (i <= 5) task.setUserId(USER_ID_1);
            else task.setUserId(USER_ID_2);
            taskNew.add(task);
        }
        TASK_SERVICE.add(taskNew);
        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Task> tasks = TASK_SERVICE.findAll();
        @NotNull final List<Task> tasks2 = TASK_SERVICE.findAll(USER_ID_1);
        Assert.assertEquals(tasks.size(), TASK_SERVICE.getSize());
        Assert.assertEquals(tasks2.size(), TASK_SERVICE.getSize(USER_ID_1));
    }

    @Test
    public void testFindAllSort() {
        @NotNull final String sortType = "BY_NAME";
        @NotNull final Sort sort = Sort.toSort(sortType);
        @NotNull final List<Task> tasksSort = TASK_SERVICE.findAll(USER_ID_1, sort.getComparator());
        Assert.assertNotNull(TASK_SERVICE.findAll(sort.getComparator()));
        Assert.assertEquals(TASK_SERVICE.findAll(USER_ID_1), tasksSort);
    }

    @Test
    public void testFindOneById() {
        @NotNull final List<Task> tasks = TASK_SERVICE.findAll(USER_ID_1);
        @NotNull final Task task1 = tasks.get(0);
        @NotNull final String taskId = task1.getId();
        Assert.assertEquals(task1, TASK_SERVICE.findOneById(taskId));
    }

    @Test
    public void testFindOneByIndex() {
        @NotNull final Task task1 = TASK_LIST.get(0);
        @NotNull final Task task2 = TASK_LIST.get(TASK_SERVICE.getSize() - 1);
        @NotNull final Task task3 = TASK_LIST.get(TASK_SERVICE.getSize() / 2);
        Assert.assertEquals(task1, TASK_SERVICE.findOneByIndex(0));
        Assert.assertEquals(task2, TASK_SERVICE.findOneByIndex(TASK_SERVICE.getSize() - 1));
        Assert.assertEquals(task3, TASK_SERVICE.findOneByIndex(TASK_SERVICE.getSize() / 2));
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        TASK_SERVICE.clear();
        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize());
    }

    @Test
    public void testClearUser() {
        final int expectedNumberOfEntries = 0;
        TASK_SERVICE.clear(USER_ID_1);
        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize(USER_ID_1));
    }

    @Test
    public void testRemove() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Task task = TASK_LIST.get(0);
        TASK_SERVICE.remove(task);
        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize());
    }

    @Test
    public void testRemoveUser() {
        final int expectedNumberOfEntries = TASK_SERVICE.getSize(USER_ID_1) - 1;
        @NotNull final Task task = TASK_LIST.get(0);
        TASK_SERVICE.remove(USER_ID_1, task);
        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize(USER_ID_1));
        Assert.assertNull(TASK_SERVICE.remove(USER_ID_1, null));
    }

    @Test
    public void testRemoveById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Task task = TASK_LIST.get(0);
        TASK_SERVICE.removeById(task.getId());
        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize());
    }

    @Test
    public void testRemoveByIdUserNegative() {
        @NotNull final Task task = TASK_LIST.get(0);
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.removeById(USER_ID_1, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.removeById(null, task.getId()));
        Assert.assertNull(TASK_SERVICE.removeById(USER_ID_FAKE, task.getId()));
    }

    @Test
    public void testRemoveByIndex() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        TASK_SERVICE.removeByIndex(0);
        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize());
    }

    @Test
    public void testRemoveByIndexUser() {
        final int expectedNumberOfEntries = TASK_SERVICE.getSize(USER_ID_1) - 1;
        TASK_SERVICE.removeByIndex(USER_ID_1, 0);
        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize(USER_ID_1));
        Assert.assertNull(TASK_SERVICE.removeByIndex(USER_ID_FAKE, 1));
    }

    @Test
    public void testRemoveAll() {
        final int expectedNumberOfEntries = 0;
        TASK_SERVICE.removeAll(TASK_LIST);
        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize());
    }

    @Test
    public void testSet() {
        @NotNull List<Task> tasks = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            tasks.add(new Task());
        }
        TASK_SERVICE.set(tasks);
        Assert.assertEquals(tasks, TASK_SERVICE.findAll());
    }

    @Test
    public void testTaskFindAllByProjectId() {
        @NotNull final Project project = PROJECT_LIST.get(0);
        @NotNull final List<Task> tasks = TASK_SERVICE.findAllByProjectId(USER_ID_1, project.getId());
        Assert.assertNotNull(tasks);
    }

    @Test
    public void testCreateNameNull() {
        @NotNull final String userId = USER_ID_1;
        @Nullable final String taskName = null;
        @NotNull final String desc = "desc";
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.create(userId, taskName));
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.create(userId, taskName, desc));
    }

    @Test
    public void testCreateUserIdNull() {
        @Nullable final String userId = null;
        @NotNull final String taskName = "TaskNullId";
        @NotNull final String desc = "desc";
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.create(userId, taskName));
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.create(userId, taskName, desc));
    }

    @Test
    public void testCreateDescriptionNull() {
        @NotNull final String userId = USER_ID_1;
        @NotNull final String taskName = "TaskNullId";
        @Nullable final String desc = null;
        Assert.assertThrows(DescriptionEmptyException.class, () -> TASK_SERVICE.create(userId, taskName, desc));
    }

    @Test
    public void testUpdateById() {
        @NotNull final String userId = USER_ID_1;
        @NotNull final Task task = TASK_LIST.get(0);
        @NotNull final String name = "TaskUpdate";
        @NotNull final String desc = "descUpdate";
        TASK_SERVICE.updateById(userId, task.getId(), name, desc);
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(desc, task.getDescription());
    }

    @Test
    public void testUpdateByIdNegative() {
        @NotNull final String userId = USER_ID_1;
        @NotNull final String userIdFake = USER_ID_FAKE;
        @NotNull final Task task = TASK_LIST.get(0);
        @NotNull final String name = "TaskUpdate";
        @NotNull final String desc = "descUpdate";
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.updateById(null, task.getId(), name, desc));
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.updateById(userId, null, name, desc));
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.updateById(userId, task.getId(), null, desc));
        Assert.assertThrows(DescriptionEmptyException.class, () -> TASK_SERVICE.updateById(userId, task.getId(), name, null));
        Assert.assertThrows(TaskNotFoundException.class, () -> TASK_SERVICE.updateById(userIdFake, task.getId(), name, desc));
    }

    @Test
    public void testUpdateByIndex() {
        @NotNull final String userId = USER_ID_1;
        @NotNull final Task task = TASK_LIST.get(0);
        @NotNull final String name = "ProjectUpdateIndex";
        @NotNull final String desc = "descUpdateIndex";
        @NotNull final Integer index = 0;
        TASK_SERVICE.updateByIndex(userId, index, name, desc);
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(desc, task.getDescription());
    }

    @Test
    public void testUpdateByIndexNegative() {
        @NotNull final String userId = USER_ID_1;
        @NotNull final String userIdFake = USER_ID_FAKE;
        @NotNull final String name = "ProjectUpdateIndex";
        @NotNull final String desc = "descUpdateIndex";
        @NotNull final Integer index = 0;
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.updateByIndex(null, index, name, desc));
        Assert.assertThrows(IndexIncorrectException.class, () -> TASK_SERVICE.updateByIndex(userId, null, name, desc));
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.updateByIndex(userId, index, null, desc));
        Assert.assertThrows(DescriptionEmptyException.class, () -> TASK_SERVICE.updateByIndex(userId, index, name, null));
        Assert.assertThrows(TaskNotFoundException.class, () -> TASK_SERVICE.updateByIndex(userIdFake, index, name, desc));
    }

    @Test
    public void testChangeProjectStatusById() {
        @NotNull final String userId = USER_ID_1;
        @NotNull final Task task = TASK_LIST.get(0);
        @NotNull final Status status = Status.IN_PROGRESS;
        TASK_SERVICE.changeTaskStatusById(userId, task.getId(), status);
        Assert.assertEquals(status, task.getStatus());
    }

    @Test
    public void testChangeTaskStatusByIdNegative() {
        @NotNull final String userId = USER_ID_1;
        @NotNull final String userIdFake = USER_ID_FAKE;
        @NotNull final Task task = TASK_LIST.get(0);
        @NotNull final Status status = Status.IN_PROGRESS;
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.changeTaskStatusById(null, task.getId(), status));
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.changeTaskStatusById(userId, null, status));
        Assert.assertThrows(TaskNotFoundException.class, () -> TASK_SERVICE.changeTaskStatusById(userIdFake, task.getId(), status));
    }

    @Test
    public void testChangeProjectStatusByIndex() {
        @NotNull final String userId = USER_ID_1;
        @NotNull final Task task = TASK_LIST.get(0);
        @NotNull final Status status = Status.IN_PROGRESS;
        @NotNull final Integer index = 0;
        TASK_SERVICE.changeTaskStatusByIndex(userId, index, status);
        Assert.assertEquals(status, task.getStatus());
    }

    @Test
    public void testChangeProjectStatusByIndexNegative() {
        @NotNull final String userId = USER_ID_1;
        @NotNull final String userIdFake = USER_ID_FAKE;
        @NotNull final Status status = Status.IN_PROGRESS;
        @NotNull final Integer index = 0;
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.changeTaskStatusByIndex(null, index, status));
        Assert.assertThrows(IndexIncorrectException.class, () -> TASK_SERVICE.changeTaskStatusByIndex(userId, null, status));
        Assert.assertThrows(TaskNotFoundException.class, () -> TASK_SERVICE.changeTaskStatusByIndex(userIdFake, index, status));
    }

}
