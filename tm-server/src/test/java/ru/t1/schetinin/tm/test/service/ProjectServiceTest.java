package ru.t1.schetinin.tm.test.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.schetinin.tm.api.repository.IProjectRepository;
import ru.t1.schetinin.tm.api.service.IProjectService;
import ru.t1.schetinin.tm.api.service.IProjectTaskService;
import ru.t1.schetinin.tm.api.service.ITaskService;
import ru.t1.schetinin.tm.api.service.IUserService;
import ru.t1.schetinin.tm.enumerated.Sort;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.schetinin.tm.exception.field.DescriptionEmptyException;
import ru.t1.schetinin.tm.exception.field.IdEmptyException;
import ru.t1.schetinin.tm.exception.field.IndexIncorrectException;
import ru.t1.schetinin.tm.exception.field.NameEmptyException;
import ru.t1.schetinin.tm.exception.user.UserIdEmptyException;
import ru.t1.schetinin.tm.marker.UnitCategory;
import ru.t1.schetinin.tm.model.Project;
import ru.t1.schetinin.tm.model.Task;
import ru.t1.schetinin.tm.repository.ProjectRepository;
import ru.t1.schetinin.tm.service.ProjectService;

import javax.validation.constraints.Null;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class ProjectServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_FAKE = UUID.randomUUID().toString();

    @NotNull
    private static final List<Project> PROJECT_LIST = new ArrayList<>();

    @NotNull
    private static final IProjectRepository PROJECT_REPOSITORY = new ProjectRepository();

    @NotNull
    private static final IProjectService PROJECT_SERVICE = new ProjectService(PROJECT_REPOSITORY);

    @Before
    public void initDemoData() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project number " + i);
            project.setDescription("Description " + i);
            if (i <= 5) project.setUserId(USER_ID_1);
            else project.setUserId(USER_ID_2);
            PROJECT_REPOSITORY.add(project);
            PROJECT_LIST.add(project);
        }
    }

    @After
    public void clearData() {
        PROJECT_REPOSITORY.clear();
        PROJECT_LIST.clear();
    }

    @Test
    public void testCreate() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = USER_ID_1;
        @NotNull final String projectName = "project-test-1";
        @NotNull final Project project = PROJECT_SERVICE.create(userId, projectName);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
        Assert.assertEquals(userId, project.getUserId());
        Assert.assertEquals(projectName, project.getName());
    }

    @Test
    public void testCreateWithDesc() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = USER_ID_1;
        @NotNull final String projectName = "project-test-1";
        @NotNull final String description = "desc project 1";
        @NotNull final Project project = PROJECT_SERVICE.create(userId, projectName, description);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
        Assert.assertEquals(userId, project.getUserId());
        Assert.assertEquals(projectName, project.getName());
        Assert.assertEquals(description, project.getDescription());
    }

    @Test
    public void testUpdateById() {
        @NotNull final String userId = USER_ID_1;
        @NotNull final Project project = PROJECT_LIST.get(0);
        @NotNull final String name = "ProjectUpdate";
        @NotNull final String desc = "descUpdate";
        PROJECT_SERVICE.updateById(userId, project.getId(), name, desc);
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(desc, project.getDescription());
    }

    @Test
    public void testUpdateByIndex() {
        @NotNull final String userId = USER_ID_1;
        @NotNull final Project project = PROJECT_LIST.get(0);
        @NotNull final String name = "ProjectUpdateIndex";
        @NotNull final String desc = "descUpdateIndex";
        @NotNull final Integer index = 0;
        PROJECT_SERVICE.updateByIndex(userId, index, name, desc);
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(desc, project.getDescription());
    }

    @Test
    public void testChangeProjectStatusById() {
        @NotNull final String userId = USER_ID_1;
        @NotNull final Project project = PROJECT_LIST.get(0);
        @NotNull final Status status = Status.IN_PROGRESS;
        PROJECT_SERVICE.changeProjectStatusById(userId, project.getId(), status);
        Assert.assertEquals(status, project.getStatus());
    }

    @Test
    public void testChangeProjectStatusByIndex() {
        @NotNull final String userId = USER_ID_1;
        @NotNull final Project project = PROJECT_LIST.get(0);
        @NotNull final Status status = Status.IN_PROGRESS;
        @NotNull final Integer index = 0;
        PROJECT_SERVICE.changeProjectStatusByIndex(userId, index, status);
        Assert.assertEquals(status, project.getStatus());
    }

    @Test
    public void testCreateNameNull() {
        @NotNull final String userId = USER_ID_1;
        @Nullable final String projectName = null;
        @NotNull final String desc = "desc";
        Assert.assertThrows(NameEmptyException.class, () -> PROJECT_SERVICE.create(userId, projectName));
        Assert.assertThrows(NameEmptyException.class, () -> PROJECT_SERVICE.create(userId, projectName, desc));
    }

    @Test
    public void testCreateUserIdNull() {
        @Nullable final String userId = null;
        @NotNull final String projectName = "ProjectNullId";
        @NotNull final String desc = "desc";
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.create(userId, projectName));
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.create(userId, projectName, desc));
    }

    @Test
    public void testCreateDescriptionNull() {
        @NotNull final String userId = USER_ID_1;
        @NotNull final String projectName = "ProjectNullId";
        @Nullable final String desc = null;
        Assert.assertThrows(DescriptionEmptyException.class, () -> PROJECT_SERVICE.create(userId, projectName, desc));
    }

    @Test
    public void testUpdateByIdNegative() {
        @NotNull final String userId = USER_ID_1;
        @NotNull final String userIdFake = USER_ID_FAKE;
        @NotNull final Project project = PROJECT_LIST.get(0);
        @NotNull final String name = "ProjectUpdate";
        @NotNull final String desc = "descUpdate";
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.updateById(null, project.getId(), name, desc));
        Assert.assertThrows(IdEmptyException.class, () -> PROJECT_SERVICE.updateById(userId, null, name, desc));
        Assert.assertThrows(NameEmptyException.class, () -> PROJECT_SERVICE.updateById(userId, project.getId(), null, desc));
        Assert.assertThrows(DescriptionEmptyException.class, () -> PROJECT_SERVICE.updateById(userId, project.getId(), name, null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> PROJECT_SERVICE.updateById(userIdFake, project.getId(), name, desc));
    }

    @Test
    public void testUpdateByIndexNegative() {
        @NotNull final String userId = USER_ID_1;
        @NotNull final String userIdFake = USER_ID_FAKE;
        @NotNull final String name = "ProjectUpdateIndex";
        @NotNull final String desc = "descUpdateIndex";
        @NotNull final Integer index = 0;
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.updateByIndex(null, index, name, desc));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.updateByIndex(userId, null, name, desc));
        Assert.assertThrows(NameEmptyException.class, () -> PROJECT_SERVICE.updateByIndex(userId, index, null, desc));
        Assert.assertThrows(DescriptionEmptyException.class, () -> PROJECT_SERVICE.updateByIndex(userId, index, name, null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> PROJECT_SERVICE.updateByIndex(userIdFake, index, name, desc));
    }

    @Test
    public void testChangeProjectStatusByIdNegative() {
        @NotNull final String userId = USER_ID_1;
        @NotNull final String userIdFake = USER_ID_FAKE;
        @NotNull final Project project = PROJECT_LIST.get(0);
        @NotNull final Status status = Status.IN_PROGRESS;
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.changeProjectStatusById(null, project.getId(), status));
        Assert.assertThrows(IdEmptyException.class, () -> PROJECT_SERVICE.changeProjectStatusById(userId, null, status));
        Assert.assertThrows(ProjectNotFoundException.class, () -> PROJECT_SERVICE.changeProjectStatusById(userIdFake, project.getId(), status));
    }

    @Test
    public void testChangeProjectStatusByIndexNegative() {
        @NotNull final String userId = USER_ID_1;
        @NotNull final String userIdFake = USER_ID_FAKE;
        @NotNull final Status status = Status.IN_PROGRESS;
        @NotNull final Integer index = 0;
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.changeProjectStatusByIndex(null, index, status));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.changeProjectStatusByIndex(userId, null, status));
        Assert.assertThrows(ProjectNotFoundException.class, () -> PROJECT_SERVICE.changeProjectStatusByIndex(userIdFake, index, status));
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        PROJECT_SERVICE.clear();
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
    }

    @Test
    public void testClearUser() {
        final int expectedNumberOfEntries = 0;
        PROJECT_SERVICE.clear(USER_ID_1);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize(USER_ID_1));
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Project> projects = PROJECT_SERVICE.findAll();
        @NotNull final List<Project> projects2 = PROJECT_SERVICE.findAll(USER_ID_1);
        Assert.assertEquals(projects.size(), PROJECT_SERVICE.getSize());
        Assert.assertEquals(projects2.size(), PROJECT_SERVICE.getSize(USER_ID_1));
    }

    @Test
    public void testAddAll() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull final List<Project> projectsNew = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project number" + i * 2);
            project.setDescription("Description " + i * 2);
            if (i <= 5) project.setUserId(USER_ID_1);
            else project.setUserId(USER_ID_2);
            projectsNew.add(project);
        }
        PROJECT_SERVICE.add(projectsNew);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
    }

    @Test
    public void testAddProject() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        final int expectedNumberOfEntriesUser = PROJECT_SERVICE.getSize(USER_ID_1) + 1;
        @NotNull final String userId = USER_ID_1;
        @NotNull final Project project = new Project();
        PROJECT_SERVICE.add(project);
        PROJECT_SERVICE.add(userId, project);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
        Assert.assertEquals(expectedNumberOfEntriesUser, PROJECT_SERVICE.getSize(USER_ID_1));
        Assert.assertNull(PROJECT_SERVICE.add(userId,null));
    }

    @Test
    public void testFindAllSort() {
        @NotNull final String sortType = "BY_NAME";
        @NotNull final Sort sort = Sort.toSort(sortType);
        @NotNull final List<Project> projectSort = PROJECT_SERVICE.findAll(USER_ID_1, sort.getComparator());
        Assert.assertNotNull(PROJECT_SERVICE.findAll(sort.getComparator()));
        Assert.assertEquals(PROJECT_SERVICE.findAll(USER_ID_1), projectSort);
    }

    @Test
    public void testExistById() {
        @NotNull final Project project = PROJECT_LIST.get(0);
        Assert.assertTrue(PROJECT_SERVICE.existsById(project.getId()));
        Assert.assertTrue(PROJECT_SERVICE.existsById(project.getUserId(), project.getId()));
    }

    @Test
    public void testFindOneById() {
        @NotNull final List<Project> projects = PROJECT_SERVICE.findAll(USER_ID_1);
        @NotNull final Project project1 = projects.get(0);
        @NotNull final String projectId = project1.getId();
        Assert.assertEquals(project1, PROJECT_SERVICE.findOneById(projectId));
    }

    @Test
    public void testFindOneByIndex() {
        @NotNull final Project project1 = PROJECT_LIST.get(0);
        @NotNull final Project project2 = PROJECT_LIST.get(PROJECT_SERVICE.getSize() - 1);
        @NotNull final Project project3 = PROJECT_LIST.get(PROJECT_SERVICE.getSize() / 2);
        Assert.assertEquals(project1, PROJECT_SERVICE.findOneByIndex(0));
        Assert.assertEquals(project2, PROJECT_SERVICE.findOneByIndex(PROJECT_SERVICE.getSize() - 1));
        Assert.assertEquals(project3, PROJECT_SERVICE.findOneByIndex(PROJECT_SERVICE.getSize() / 2));
    }

    @Test
    public void testRemove() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Project project = PROJECT_LIST.get(0);
        PROJECT_SERVICE.remove(project);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
    }

    @Test
    public void testRemoveUser() {
        final int expectedNumberOfEntries = PROJECT_SERVICE.getSize(USER_ID_1) - 1;
        @NotNull final Project project = PROJECT_LIST.get(0);
        PROJECT_SERVICE.remove(USER_ID_1, project);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize(USER_ID_1));
        Assert.assertNull(PROJECT_SERVICE.remove(USER_ID_1, null));
    }

    @Test
    public void testRemoveById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Project project = PROJECT_LIST.get(0);
        PROJECT_SERVICE.removeById(project.getId());
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
    }

    @Test
    public void testRemoveByIdUserNegative() {
        @NotNull final Project project = PROJECT_LIST.get(0);
        Assert.assertThrows(IdEmptyException.class, () -> PROJECT_SERVICE.removeById(USER_ID_1, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.removeById(null, project.getId()));
        Assert.assertNull(PROJECT_SERVICE.removeById(USER_ID_FAKE, project.getId()));
    }

    @Test
    public void testRemoveByIndex() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        PROJECT_SERVICE.removeByIndex(0);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
    }

    @Test
    public void testRemoveByIndexUser() {
        final int expectedNumberOfEntries = PROJECT_SERVICE.getSize(USER_ID_1) - 1;
        PROJECT_SERVICE.removeByIndex(USER_ID_1, 0);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize(USER_ID_1));
        Assert.assertNull(PROJECT_SERVICE.removeByIndex(USER_ID_FAKE, 1));
    }

    @Test
    public void testRemoveAll() {
        final int expectedNumberOfEntries = 0;
        PROJECT_SERVICE.removeAll(PROJECT_LIST);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
    }
}