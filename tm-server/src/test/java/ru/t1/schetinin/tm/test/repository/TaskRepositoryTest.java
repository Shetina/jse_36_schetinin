package ru.t1.schetinin.tm.test.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.schetinin.tm.api.repository.IProjectRepository;
import ru.t1.schetinin.tm.api.repository.ITaskRepository;
import ru.t1.schetinin.tm.enumerated.Sort;
import ru.t1.schetinin.tm.marker.UnitCategory;
import ru.t1.schetinin.tm.model.Project;
import ru.t1.schetinin.tm.model.Task;
import ru.t1.schetinin.tm.repository.ProjectRepository;
import ru.t1.schetinin.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class TaskRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_FAKE = UUID.randomUUID().toString();

    @NotNull
    private static final List<Task> TASK_LIST = new ArrayList<>();

    @NotNull
    private static final ITaskRepository TASK_REPOSITORY = new TaskRepository();

    @NotNull
    private static final List<Project> PROJECT_LIST = new ArrayList<>();

    @NotNull
    private static final IProjectRepository PROJECT_REPOSITORY = new ProjectRepository();

    @Before
    public void initDemoData() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            @NotNull final Task task = new Task();
            project.setName("Project number " + i);
            project.setDescription("Description " + i);
            task.setName("Task number " + i);
            task.setDescription("Description " + i);
            if (i <= 5) {
                project.setUserId(USER_ID_1);
                task.setUserId(USER_ID_1);
                task.setProjectId(project.getId());
            }
            else {
                project.setUserId(USER_ID_2);
                task.setUserId(USER_ID_2);
                task.setProjectId(project.getId());
            }
            PROJECT_REPOSITORY.add(project);
            PROJECT_LIST.add(project);
            TASK_REPOSITORY.add(task);
            TASK_LIST.add(task);
        }
    }

    @After
    public void clearData() {
        TASK_REPOSITORY.clear();
        TASK_LIST.clear();
    }

    @Test
    public void testAddTask() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        final int expectedNumberOfEntriesUser = TASK_REPOSITORY.getSize(USER_ID_1) + 1;
        @NotNull final String userId = USER_ID_1;
        @NotNull final Task task = new Task();
        TASK_REPOSITORY.add(task);
        TASK_REPOSITORY.add(userId, task);
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize());
        Assert.assertEquals(expectedNumberOfEntriesUser, TASK_REPOSITORY.getSize(USER_ID_1));
        Assert.assertNull(TASK_REPOSITORY.add(userId,null));
    }

    @Test
    public void testExistById() {
        @NotNull final Task task = TASK_LIST.get(0);
        Assert.assertTrue(TASK_REPOSITORY.existsById(task.getId()));
        Assert.assertTrue(TASK_REPOSITORY.existsById(task.getUserId(), task.getId()));
    }

    @Test
    public void testCreateTask() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = USER_ID_1;
        @NotNull final String taskName = "Task create 1";
        @NotNull final Task task = TASK_REPOSITORY.create(userId, taskName);
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize());
        Assert.assertEquals(userId, task.getUserId());
        Assert.assertEquals(taskName, task.getName());
    }

    @Test
    public void testCreateTaskWithDesc() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = USER_ID_1;
        @NotNull final String taskName = "Task create 1";
        @NotNull final String description = "Desc task 1";
        @NotNull final Task task = TASK_REPOSITORY.create(userId, taskName, description);
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize());
        Assert.assertEquals(userId, task.getUserId());
        Assert.assertEquals(taskName, task.getName());
        Assert.assertEquals(description, task.getDescription());
    }

    @Test
    public void testAddAll() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull final List<Task> taskNew = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task number" + i * 2);
            task.setDescription("Description " + i * 2);
            if (i <= 5) task.setUserId(USER_ID_1);
            else task.setUserId(USER_ID_2);
            taskNew.add(task);
        }
        TASK_REPOSITORY.add(taskNew);
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Task> tasks = TASK_REPOSITORY.findAll();
        @NotNull final List<Task> tasks2 = TASK_REPOSITORY.findAll(USER_ID_1);
        Assert.assertEquals(tasks.size(), TASK_REPOSITORY.getSize());
        Assert.assertEquals(tasks2.size(), TASK_REPOSITORY.getSize(USER_ID_1));
    }

    @Test
    public void testFindAllSort() {
        @NotNull final String sortType = "BY_NAME";
        @NotNull final Sort sort = Sort.toSort(sortType);
        @NotNull final List<Task> tasksSort = TASK_REPOSITORY.findAll(USER_ID_1, sort.getComparator());
        Assert.assertNotNull(TASK_REPOSITORY.findAll(sort.getComparator()));
        Assert.assertEquals(TASK_REPOSITORY.findAll(USER_ID_1), tasksSort);
    }

    @Test
    public void testFindOneById() {
        @NotNull final List<Task> tasks = TASK_REPOSITORY.findAll(USER_ID_1);
        @NotNull final Task task1 = tasks.get(0);
        @NotNull final String taskId = task1.getId();
        Assert.assertEquals(task1, TASK_REPOSITORY.findOneById(taskId));
    }

    @Test
    public void testFindOneByIndex() {
        @NotNull final Task task1 = TASK_LIST.get(0);
        @NotNull final Task task2 = TASK_LIST.get(TASK_REPOSITORY.getSize() - 1);
        @NotNull final Task task3 = TASK_LIST.get(TASK_REPOSITORY.getSize() / 2);
        Assert.assertEquals(task1, TASK_REPOSITORY.findOneByIndex(0));
        Assert.assertEquals(task2, TASK_REPOSITORY.findOneByIndex(TASK_REPOSITORY.getSize() - 1));
        Assert.assertEquals(task3, TASK_REPOSITORY.findOneByIndex(TASK_REPOSITORY.getSize() / 2));
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        TASK_REPOSITORY.clear();
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize());
    }

    @Test
    public void testClearUser() {
        final int expectedNumberOfEntries = 0;
        TASK_REPOSITORY.clear(USER_ID_1);
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize(USER_ID_1));
    }

    @Test
    public void testRemove() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Task task = TASK_LIST.get(0);
        TASK_REPOSITORY.remove(task);
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize());
    }

    @Test
    public void testRemoveUser() {
        final int expectedNumberOfEntries = TASK_REPOSITORY.getSize(USER_ID_1) - 1;
        @NotNull final Task task = TASK_LIST.get(0);
        TASK_REPOSITORY.remove(USER_ID_1, task);
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize(USER_ID_1));
        Assert.assertNull(TASK_REPOSITORY.remove(USER_ID_1, null));
    }

    @Test
    public void testRemoveById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Task task = TASK_LIST.get(0);
        TASK_REPOSITORY.removeById(task.getId());
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize());
    }

    @Test
    public void testRemoveByIdUserNegative() {
        @NotNull final Task task = TASK_LIST.get(0);
        Assert.assertNull(TASK_REPOSITORY.removeById(USER_ID_1, null));
        Assert.assertNull(TASK_REPOSITORY.removeById(USER_ID_FAKE, task.getId()));
    }

    @Test
    public void testRemoveByIndex() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        TASK_REPOSITORY.removeByIndex(0);
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize());
    }

    @Test
    public void testRemoveByIndexUser() {
        final int expectedNumberOfEntries = TASK_REPOSITORY.getSize(USER_ID_1) - 1;
        TASK_REPOSITORY.removeByIndex(USER_ID_1, 0);
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize(USER_ID_1));
        Assert.assertNull(TASK_REPOSITORY.removeByIndex(USER_ID_FAKE, 1));
    }

    @Test
    public void testRemoveAll() {
        final int expectedNumberOfEntries = 0;
        TASK_REPOSITORY.removeAll(TASK_LIST);
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.getSize());
    }

    @Test
    public void testSet() {
        @NotNull List<Task> tasks = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            tasks.add(new Task());
        }
        TASK_REPOSITORY.set(tasks);
        Assert.assertEquals(tasks, TASK_REPOSITORY.findAll());
    }

    @Test
    public void testTaskFindAllByProjectId() {
        @NotNull final Project project = PROJECT_LIST.get(0);
        @NotNull final List<Task> tasks = TASK_REPOSITORY.findAllByProjectId(USER_ID_1, project.getId());
        Assert.assertNotNull(tasks);
    }

}