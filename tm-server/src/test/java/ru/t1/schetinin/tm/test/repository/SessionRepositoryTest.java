package ru.t1.schetinin.tm.test.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.schetinin.tm.api.repository.ISessionRepository;
import ru.t1.schetinin.tm.marker.UnitCategory;
import ru.t1.schetinin.tm.model.Project;
import ru.t1.schetinin.tm.model.Session;
import ru.t1.schetinin.tm.repository.SessionRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class SessionRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    private static final String USER_ID_FAKE = UUID.randomUUID().toString();

    @NotNull
    private static final List<Session> SESSION_LIST = new ArrayList<>();

    @NotNull
    private static final ISessionRepository SESSION_REPOSITORY = new SessionRepository();

    @Before
    public void initDemoData() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = new Session();
            if (i <= 5) session.setUserId(USER_ID_1);
            else session.setUserId(USER_ID_2);
            SESSION_REPOSITORY.add(session);
            SESSION_LIST.add(session);
        }
    }

    @After
    public void clearData() {
        SESSION_LIST.clear();
        SESSION_REPOSITORY.clear();
    }

    @Test
    public void testAddSession() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        final int expectedNumberOfEntriesUser = SESSION_REPOSITORY.getSize(USER_ID_1) + 1;
        @NotNull final String userId = USER_ID_1;
        @NotNull final Session session = new Session();
        SESSION_REPOSITORY.add(session);
        SESSION_REPOSITORY.add(userId, session);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize());
        Assert.assertEquals(expectedNumberOfEntriesUser, SESSION_REPOSITORY.getSize(USER_ID_1));
        Assert.assertNull(SESSION_REPOSITORY.add(userId,null));
    }

    @Test
    public void testExistById() {
        @NotNull final Session session = SESSION_LIST.get(0);
        Assert.assertTrue(SESSION_REPOSITORY.existsById(session.getId()));
        Assert.assertTrue(SESSION_REPOSITORY.existsById(session.getUserId(), session.getId()));
    }

    @Test
    public void testAddAll() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull final List<Session> sessionNew = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = new Session();
            if (i <= 5) session.setUserId(USER_ID_1);
            else session.setUserId(USER_ID_2);
            sessionNew.add(session);
        }
        SESSION_REPOSITORY.add(sessionNew);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Session> sessions = SESSION_REPOSITORY.findAll();
        @NotNull final List<Session> sessions2 = SESSION_REPOSITORY.findAll(USER_ID_1);
        Assert.assertEquals(sessions.size(), SESSION_REPOSITORY.getSize());
        Assert.assertEquals(sessions2.size(), SESSION_REPOSITORY.getSize(USER_ID_1));
    }

    @Test
    public void testFindOneById() {
        @NotNull final List<Session> sessions = SESSION_REPOSITORY.findAll(USER_ID_1);
        @NotNull final Session session1 = sessions.get(0);
        @NotNull final String projectId = session1.getId();
        Assert.assertEquals(session1, SESSION_REPOSITORY.findOneById(projectId));
    }

    @Test
    public void testFindOneByIndex() {
        @NotNull final Session session1 = SESSION_LIST.get(0);
        @NotNull final Session session2 = SESSION_LIST.get(SESSION_REPOSITORY.getSize() - 1);
        @NotNull final Session session3 = SESSION_LIST.get(SESSION_REPOSITORY.getSize() / 2);
        Assert.assertEquals(session1, SESSION_REPOSITORY.findOneByIndex(0));
        Assert.assertEquals(session2, SESSION_REPOSITORY.findOneByIndex(SESSION_REPOSITORY.getSize() - 1));
        Assert.assertEquals(session3, SESSION_REPOSITORY.findOneByIndex(SESSION_REPOSITORY.getSize() / 2));
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        SESSION_REPOSITORY.clear();
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize());
    }

    @Test
    public void testClearUser() {
        final int expectedNumberOfEntries = 0;
        SESSION_REPOSITORY.clear(USER_ID_1);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize(USER_ID_1));
    }


    @Test
    public void testRemove() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Session session = SESSION_LIST.get(0);
        SESSION_REPOSITORY.remove(session);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize());
    }

    @Test
    public void testRemoveUser() {
        final int expectedNumberOfEntries = SESSION_REPOSITORY.getSize(USER_ID_1) - 1;
        @NotNull final Session session = SESSION_LIST.get(0);
        SESSION_REPOSITORY.remove(USER_ID_1, session);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize(USER_ID_1));
        Assert.assertNull(SESSION_REPOSITORY.remove(USER_ID_1, null));
    }

    @Test
    public void testRemoveById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Session session = SESSION_LIST.get(0);
        SESSION_REPOSITORY.removeById(session.getId());
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize());
    }

    @Test
    public void testRemoveByIdUserNegative() {
        @NotNull final Session session = SESSION_LIST.get(0);
        Assert.assertNull(SESSION_REPOSITORY.removeById(USER_ID_1, null));
        Assert.assertNull(SESSION_REPOSITORY.removeById(USER_ID_FAKE, session.getId()));
    }

    @Test
    public void testRemoveByIndex() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        SESSION_REPOSITORY.removeByIndex(0);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize());
    }

    @Test
    public void testRemoveByIndexUser() {
        final int expectedNumberOfEntries = SESSION_REPOSITORY.getSize(USER_ID_1) - 1;
        SESSION_REPOSITORY.removeByIndex(USER_ID_1, 0);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize(USER_ID_1));
        Assert.assertNull(SESSION_REPOSITORY.removeByIndex(USER_ID_FAKE, 1));
    }

    @Test
    public void testRemoveAll() {
        final int expectedNumberOfEntries = 0;
        SESSION_REPOSITORY.removeAll(SESSION_LIST);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize());
    }

    @Test
    public void testSet() {
        @NotNull List<Session> sessions = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            sessions.add(new Session());
        }
        SESSION_REPOSITORY.set(sessions);
        Assert.assertEquals(sessions, SESSION_REPOSITORY.findAll());
    }

}
