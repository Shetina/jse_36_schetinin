package ru.t1.schetinin.tm.endpoint;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.schetinin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.schetinin.tm.api.endpoint.IDomainEndpoint;
import ru.t1.schetinin.tm.api.service.IPropertyService;
import ru.t1.schetinin.tm.dto.request.*;
import ru.t1.schetinin.tm.marker.SoapCategory;
import ru.t1.schetinin.tm.service.PropertyService;

@Category(SoapCategory.class)
public class DomainEndpointTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IAuthEndpoint AUTH_ENDPOINT_CLIENT = IAuthEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final IDomainEndpoint DOMAIN_ENDPOINT_CLIENT = IDomainEndpoint.newInstance(PROPERTY_SERVICE);

    @Nullable
    private static String adminToken;

    @BeforeClass
    public static void setConnection() {
        @NotNull final UserLoginRequest loginRequestAdmin = new UserLoginRequest();
        loginRequestAdmin.setLogin("admin");
        loginRequestAdmin.setPassword("admin");
        adminToken = AUTH_ENDPOINT_CLIENT.login(loginRequestAdmin).getToken();
        Assert.assertNotNull(adminToken);
    }

    @Test
    public void testSaveBackup() {
        @NotNull final DataBackupSaveRequest request = new DataBackupSaveRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.saveDataBackup(request));
    }

    @Test
    public void testLoadBackup() {
        @NotNull final DataBackupLoadRequest request = new DataBackupLoadRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.loadDataBackup(request));
    }

    @Test
    public void testSaveBase64() {
        @NotNull final DataBase64SaveRequest request = new DataBase64SaveRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.saveDataBase64(request));
    }

    @Test
    public void testLoadBase64() {
        @NotNull final DataBase64LoadRequest request = new DataBase64LoadRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.loadDataBase64(request));
    }

    @Test
    public void testSaveBinary() {
        @NotNull final DataBinarySaveRequest request = new DataBinarySaveRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.saveDataBinary(request));
    }

    @Test
    public void testSaveJsonFasterXml() {
        @NotNull final DataJsonSaveFasterXmlRequest request = new DataJsonSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.saveDataJsonFasterXml(request));
    }

    @Test
    public void testLoadJsonFasterXml() {
        @NotNull final DataJsonLoadFasterXmlRequest request = new DataJsonLoadFasterXmlRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.loadDataJsonFasterXml(request));
    }

    @Test
    public void testSaveXmlFasterXml() {
        @NotNull final DataXmlSaveFasterXmlRequest request = new DataXmlSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.saveDataXmlFasterXml(request));
    }

    @Test
    public void testLoadXmlFasterXml() {
        @NotNull final DataXmlLoadFasterXmlRequest request = new DataXmlLoadFasterXmlRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.loadDataXmlFasterXml(request));
    }

    @Test
    public void testSaveJsonJaxB() {
        @NotNull final DataJsonSaveJaxBRequest request = new DataJsonSaveJaxBRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.saveDataJsonJaxB(request));
    }

    @Test
    public void testSaveXmlJaxB() {
        @NotNull final DataXmlSaveJaxBRequest request = new DataXmlSaveJaxBRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.saveDataXmlJaxB(request));
    }

    @Test
    public void testSaveYaml() {
        @NotNull final DataYamlSaveFasterXmlRequest request = new DataYamlSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.saveDataYamlFasterXml(request));
    }

    @Test
    public void testLoadYaml() {
        @NotNull final DataYamlLoadFasterXmlRequest request = new DataYamlLoadFasterXmlRequest(adminToken);
        Assert.assertNotNull(DOMAIN_ENDPOINT_CLIENT.loadDataYamlFasterXml(request));
    }

}