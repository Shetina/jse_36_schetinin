package ru.t1.schetinin.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class DataBackupLoadRequest extends AbstractUserRequest {

    public DataBackupLoadRequest(@Nullable final String token) {
        super(token);
    }

}