package ru.t1.schetinin.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.model.Task;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TaskShowByProjectIdResponse extends AbstractTaskResponse {

    @Nullable
    private List<Task> tasks;

    public TaskShowByProjectIdResponse(@Nullable final List<Task> tasks) {
        this.tasks = tasks;
    }

}