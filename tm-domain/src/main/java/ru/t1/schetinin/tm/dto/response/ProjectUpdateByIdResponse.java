package ru.t1.schetinin.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.model.Project;

@Getter
@Setter
@NoArgsConstructor
public class ProjectUpdateByIdResponse extends AbstractProjectResponse {

    public ProjectUpdateByIdResponse(@NotNull final Project project) {
        super(project);
    }

}