package ru.t1.schetinin.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class DataXmlSaveFasterXmlRequest extends AbstractUserRequest {

    public DataXmlSaveFasterXmlRequest(@Nullable final String token) {
        super(token);
    }

}